package com.example.firebasemessaging;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;

import java.util.Random;

import static android.content.Context.NOTIFICATION_SERVICE;


public class NotificationsUtils {
    public static final String EXTRA_INTENT = "pendingIntent";
    public static final String EXTRA_ID_NOTIFICATION = "idNotification";
    private static String SILENT_CHANNEL = "channel";

    public static void createNotificationWithIntent(Context context,
                                                    String title, String description,
                                                    PendingIntent pendingIntent, int idNotification) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(SILENT_CHANNEL, "Channel Id",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setShowBadge(true);

            notificationManager.createNotificationChannel(channel);
            Notification.Builder builder = new Notification.Builder(context, SILENT_CHANNEL);

            NotificationStandartWithIntent(notificationManager, builder, context, pendingIntent, idNotification, title, description);
        } else {

            Notification.Builder builder = new Notification.Builder(context);


            NotificationStandartWithIntent(notificationManager, builder, context, pendingIntent, idNotification, title, description);
        }


    }

    public static void createNotificationBigImage(Context context, String title, String description, Bitmap urlImage) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(SILENT_CHANNEL, "Channel Id",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setShowBadge(true);

            notificationManager.createNotificationChannel(channel);

            Notification.Builder builder = new Notification.Builder(context, SILENT_CHANNEL);

            NotificationBigImage(notificationManager, builder,
                    context, title, description, urlImage);
        } else {
            Notification.Builder builder = new Notification.Builder(context);

            NotificationBigImage(notificationManager, builder, context, title, description, urlImage);
        }
    }

    public static void createSimpleNotification(Context context, String title, String description) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(SILENT_CHANNEL, "Channel Id",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setShowBadge(true);

            notificationManager.createNotificationChannel(channel);

            Notification.Builder builder = new Notification.Builder(context, SILENT_CHANNEL);

            NotificationStandart(notificationManager, builder, context, title, description);
        } else {
            Notification.Builder builder = new Notification.Builder(context);

            NotificationStandart(notificationManager, builder, context, title, description);
        }


    }


    private static void NotificationStandart(NotificationManager notificationManager, Notification.Builder builder,
                                             Context context, String title, String description) {

        builder
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(title)
                .setContentText(description)
                .setLights(Color.BLUE, 500, 500);
//                .setVibrate(vibration)
        Notification notification = builder.build();
        notificationManager.notify(new Random().nextInt(), notification);
    }


    private static void NotificationStandartWithIntent(NotificationManager notificationManager, Notification.Builder builder, Context context,
                                                       PendingIntent contentIntent, int idNotification, String title, String description) {

        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(title)
                .setContentText(description)
                .setLights(Color.BLUE, 500, 500)
//                .setVibrate(vibration)
                .addAction(R.mipmap.ic_launcher, "Перейти", contentIntent);
        Notification notification = builder.build();
        notificationManager.notify(idNotification, notification);

    }

    private static void NotificationBigImage(NotificationManager notificationManager, Notification.Builder builder,
                                             Context context, String title, String description, Bitmap urlImage) {

        builder
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(title)
                .setContentText(description)
                .setLights(Color.BLUE, 500, 500)
                .setLargeIcon(urlImage)
                .setStyle(new Notification.BigPictureStyle()
                        .bigPicture(urlImage)
                        .bigLargeIcon(urlImage)
                );
        Notification notification = builder.build();
        notificationManager.notify(new Random().nextInt(), notification);
    }


}
